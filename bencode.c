/*
 * C implementation of a bencode decoder.
 * This is the format defined by BitTorrent:
 *  http://wiki.theory.org/BitTorrentSpecification#bencoding
 *
 * The only external requirements are a few [standard] function calls and
 * the long long type.  Any sane system should provide all of these things.
 *
 * See the bencode.h header file for usage information.
 *
 * This is released into the public domain:
 *  http://en.wikipedia.org/wiki/Public_Domain
 *
 * Written by:
 *   Mike Frysinger <vapier@gmail.com>
 * And improvements from:
 *   Gilles Chanteperdrix <gilles.chanteperdrix@xenomai.org>
 */

/*
 * This implementation isn't optimized at all as I wrote it to support a bogus
 * system.  I have no real interest in this format.  Feel free to send me
 * patches (so long as you don't copyright them and you release your changes
 * into the public domain as well).
 */

/* Nektere casti tohoto souboru musely byt upraveno pro moje zadani a pro jazyk C */

#include <stdio.h> /* printf() */
#include <stdlib.h> /* malloc() realloc() free() strtoll() */
#include <string.h> /* memset() */
#include <sys/types.h>
#include <stdint.h>
#include "bencode.h"
#include "peerlist.h"

#ifndef BE_DEBUG
#define BE_DEBUG 0
#endif

int flag;
int name;
int position;
int http_count = 0;
int bencodePeers = 0;
long long sizePeers = 0;

FILE *bencoded_file;
FILE *http_file;

static inline void dump_string(const char *str, long long len)
{
	long long i;
	const unsigned char *s = (const unsigned char *)str;

	/* Assume non-ASCII data is binary. */
	for (i = 0; i < len; ++i)
		if (s[i] >= 0x20 && s[i] <= 0x7e)
			fprintf(bencoded_file,"%c", s[i]);
		else
			fprintf(bencoded_file,"\\x%02x", s[i]);
}

/* tento kod pripsan pro prochazeni byte v odpovedi -> zpracovani IP ADRESY A PORTU */
static inline void dump_string_peers(const char *str, long long len)
{	
	long long i;
	char *ip = (char *) malloc(20);
	char *port = (char *) malloc(10);
	char *hexIp = (char *) malloc(20);
	char *hexPort = (char *) malloc(10);
	FILE *bencodePeersFile = fopen("bencode_peers.txt", "w");
	const unsigned char *s = (const unsigned char *)str;
	unsigned int p1, p2;

	sizePeers = len;

	/* decodovani vsech ip adres - byte po byte ip adresy a portu pomoci funkci */
	for (i = 0; i < len; ++i) {
		
		memset(ip, '\0', 20);
		memset(port, '\0', 10);
		/* ip adresa */
		strcpy(hexIp, "0x");
		sprintf(ip, "%02x", s[i]);
		strcat(hexIp, ip);
		sprintf(ip, "%02x", s[i+1]);
		strcat(hexIp, ip);
		sprintf(ip, "%02x", s[i+2]);
		strcat(hexIp, ip);
		sprintf(ip, "%02x", s[i+3]);
		strcat(hexIp, ip);

		intToIp(hexToInt(hexIp));	// prelozeni hexa -> int -> ip adresa

		/* port */
		sprintf(port, "%02x", s[i+4]);
		strcpy(hexPort, port);
		p1 = strtol(hexPort, (char **)NULL, 16);	// prvni dvojcisli portu
		
		sprintf(port, "%02x", s[i+5]);
		strcpy(hexPort, port);
		p2 = strtol(hexPort, (char **)NULL, 16);	// druha cast portu

		intToPort(p1, p2);		// hexa -> int -> port (P1P2)
	
		i += 5;	
	}

	free(ip);
	free(port);
	free(hexIp);
	free(hexPort);

	if (bencodePeersFile != NULL) 
		fclose(bencodePeersFile);
	
}

static void _be_dump_indent(ssize_t indent)
{
	while (indent-- > 0)
		fprintf(bencoded_file,"    ");
}
static void _be_dump(be_node *node, ssize_t indent)
{
	size_t i;

	_be_dump_indent(indent);
	indent = abs(indent);

	switch (node->type) {
		case BE_STR: {
			long long len = be_str_len(node);
			fprintf(bencoded_file,"str = ");

			if (node->val.s != NULL) { 
				if((strstr(node->val.s, "http://") != NULL) && (flag == 1)) {
					http_count++;
					fprintf(http_file,"%s||", node->val.s);
				}
			}
			//printf("str = ");
			if (bencodePeers == 0)
				dump_string(node->val.s, len);
			else
				dump_string_peers(node->val.s, len);

			fprintf(bencoded_file," (len = %lli)\n", len);
			//printf(" (len = %lli)\n", len);
			break;
		}

		case BE_INT:
			fprintf(bencoded_file,"int = %lli\n", node->val.i);
			break;

		case BE_LIST:
			fprintf(bencoded_file,"list [\n");
			flag = 1;
			for (i = 0; node->val.l[i]; ++i)
				_be_dump(node->val.l[i], indent + 1);
			flag = 0;
			_be_dump_indent(indent);
			fprintf(bencoded_file,"]\n");
			break;

		case BE_DICT:
			fprintf(bencoded_file,"dict {\n");

			for (i = 0; node->val.d[i].val; ++i) {
				_be_dump_indent(indent + 1);
				fprintf(bencoded_file,"%s => ", node->val.d[i].key);
				//printf("\n%s => ", node->val.d[i].key);
				_be_dump(node->val.d[i].val, -(indent + 1));
			}

			_be_dump_indent(indent);
			fprintf(bencoded_file,"}\n");
			break;
	}
}
void be_dump(be_node *node)
{
	name = 1;
	flag = 0;
	position = 0;
	name++;

	bencoded_file = fopen("bencoded.torrent","w+");
	http_file = fopen("http_url.txt","w");
	
	_be_dump(node, 0);
	
	if (bencoded_file != NULL) 
		fclose(bencoded_file);
	
	if (http_file != NULL) 
		fclose(http_file);
}

static be_node *be_alloc(be_type type)
{
	be_node *ret = malloc(sizeof(*ret));
	if (ret) {
		memset(ret, 0x00, sizeof(*ret));
		ret->type = type;
	}
	return ret;
}

static long long _be_decode_int(const char **data, long long *data_len)
{
	char *endp;
	long long ret = strtoll(*data, &endp, 10);
	*data_len -= (endp - *data);
	*data = endp;
	return ret;
}

long long be_str_len(be_node *node)
{
	long long ret = 0;
	if (node->val.s)
		memcpy(&ret, node->val.s - sizeof(ret), sizeof(ret));

	return ret;
}

static char *_be_decode_str(const char **data, long long *data_len)
{
	long long sllen = _be_decode_int(data, data_len);
	long slen = sllen;
	unsigned long len;
	char *ret = NULL;

	/* slen is signed, so negative values get rejected */
	if (sllen < 0)
		return ret;

	/* reject attempts to allocate large values that overflow the
	 * size_t type which is used with malloc()
	 */
	if (sizeof(long long) != sizeof(long))
		if (sllen != slen)
			return ret;

	/* make sure we have enough data left */
	if (sllen > *data_len - 1)
		return ret;

	/* switch from signed to unsigned so we don't overflow below */
	len = slen;

	if (**data == ':') {
		char *_ret = malloc(sizeof(sllen) + len + 1);
		memcpy(_ret, &sllen, sizeof(sllen));
		ret = _ret + sizeof(sllen);
		memcpy(ret, *data + 1, len);
		ret[len] = '\0';
		*data += len + 1;
		*data_len -= len + 1;
	}
	return ret;
}

static be_node *_be_decode(const char **data, long long *data_len)
{
	be_node *ret = NULL;
	if (!*data_len)
		return ret;

	switch (**data) {
		/* lists */
		case 'l': {
			unsigned int i = 0;

			ret = be_alloc(BE_LIST);

			--(*data_len);
			++(*data);
			while (**data != 'e') {
				ret->val.l = realloc(ret->val.l, (i + 2) * sizeof(*ret->val.l));
				ret->val.l[i] = _be_decode(data, data_len);
				if (!ret->val.l[i])
					break;
				++i;
			}
			--(*data_len);
			++(*data);

			/* In case of an empty list. Uncommon, but valid. */
			if (!i)
				ret->val.l = realloc(ret->val.l, sizeof(*ret->val.l));

			ret->val.l[i] = NULL;

			return ret;
		}

		/* dictionaries */
		case 'd': {
			unsigned int i = 0;

			ret = be_alloc(BE_DICT);

			--(*data_len);
			++(*data);
			while (**data != 'e') {
				ret->val.d = realloc(ret->val.d, (i + 2) * sizeof(*ret->val.d));
				ret->val.d[i].key = _be_decode_str(data, data_len);
				ret->val.d[i].val = _be_decode(data, data_len);
				if (!ret->val.l[i])
					break;
				++i;
			}
			--(*data_len);
			++(*data);

			ret->val.d[i].val = NULL;

			return ret;
		}

		/* integers */
		case 'i': {
			ret = be_alloc(BE_INT);

			--(*data_len);
			++(*data);
			ret->val.i = _be_decode_int(data, data_len);
			if (**data != 'e') {
				be_free(ret);
				return NULL;
			}
			--(*data_len);
			++(*data);
			return ret;
		}

		/* byte strings */
		case '0':
		case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9': {
			ret = be_alloc(BE_STR);
			ret->val.s = _be_decode_str(data, data_len);
			return ret;
		}

		/* invalid */
		default:
			break;
	}
	
	return ret;
}

be_node *be_decoden(const char *data, long long len)
{
	return _be_decode(&data, &len);
}

be_node *be_decode(const char *data)
{
	return be_decoden(data, strlen(data));
}

static inline void _be_free_str(char *str)
{
	if (str)
		free(str - sizeof(long long));
}
void be_free(be_node *node)
{
	switch (node->type) {
		case BE_STR:
			_be_free_str(node->val.s);
			break;

		case BE_INT:
			break;

		case BE_LIST: {
			unsigned int i;
			for (i = 0; node->val.l[i]; ++i)
				be_free(node->val.l[i]);
			free(node->val.l);
			break;
		}

		case BE_DICT: {
			unsigned int i;
			for (i = 0; node->val.d[i].val; ++i) {
				_be_free_str(node->val.d[i].key);
				be_free(node->val.d[i].val);
			}
			free(node->val.d);
			break;
		}
	}
	free(node);
}

