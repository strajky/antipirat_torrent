
CC = gcc
CFLAGS = -g -std=c99 -Wall -Wextra -pedantic -I/usr/include/libxml2 -Wno-variadic-macros
LOGIN = xhynek09
FILES = Makefile README manual.pdf *.c *.h

all:	download antipirat

rebuild:	clean all

rssfeed.o:	rssfeed.c rssfeed.h parser.h error.h 
			$(CC) $(CFLAGS) -c rssfeed.c -o rssfeed.o

bencode.o:	bencode.c bencode.h peerlist.h
			$(CC) $(CFLAGS) -c bencode.c -o bencode.o		

peerlist.o:	peerlist.c peerlist.h  parser.h bencode.h antipirat.h
			$(CC) $(CFLAGS) -c peerlist.c -o peerlist.o		

parser.o:	parser.c parser.h error.h bencode.h peerlist.h
			$(CC) $(CFLAGS) -c parser.c -o parser.o -lxml2 	

error.o:	error.c error.h
			$(CC) $(CFLAGS) -c error.c -o error.o		

antipirat.o:	antipirat.c rssfeed.h error.h parser.h bencode.h antipirat.h
				$(CC) $(CFLAGS) -c antipirat.c -o antipirat.o 

antipirat: 	rssfeed.o peerlist.o parser.o bencode.o antipirat.o error.o
			$(CC) $(CFLAGS) rssfeed.o peerlist.o parser.o error.o bencode.o antipirat.o -o antipirat -lcurl -lxml2 

download: 
		#wget https://github.com/bagder/curl/releases/download/curl-7_45_0/curl-7.45.0.tar.gz --no-clobber
		#tar -xvzf curl-7.45.0.tar.gz --skip-old-files
		#cd curl-7.45.0; \
		#./configure --enable-http --disable-ftp --disable-file --disable-ldap --disable-ldaps --disable-rtsp --disable-proxy --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smb --disable-smtp --disable-gopher --disable-manual; \
		#sudo make; \
		#sudo make install

clean:
	rm -fr *.o $(OBJFILES)

tar: clean 
	 tar -cf $(LOGIN).tar $(FILES)
