/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#ifndef rssfeed
#define rssfeed

#include <stdio.h>
#include <stdlib.h>

void getRSS(char **rss_url);

#endif