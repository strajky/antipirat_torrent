/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <curl/curl.h>
#include <fcntl.h>
#include "bencode.h"
#include "peerlist.h"
#include "parser.h"
#include "error.h"
#include "antipirat.h"
#include <errno.h>

#define h_addr h_addr_list[0]

time_t rawtime;
struct tm * timeinfo;

char *peers;
char *buf;
FILE *peerList;

/************************************************
 ****** VYPIS CHYBOVEHO HLASENI ZE SOUBORU ******/
void printFile(char *string, int i) {
	
	while(string[i] != '\0') {
		
		fprintf(stderr, "%c", string[i]);
		i++;
	}
} 
/************************************************
 ************ PREVOD HEX NA IP ADRESU ***********/ 
unsigned int hexToInt(char *hexStr) {
	
	unsigned int ip_add;
	sscanf(hexStr, "%x", &ip_add);
	//printf("%x\n", ip_add);

	return ip_add;;
}
/************************************************
 ********** PREVOD CISLA NA CELY PORT ***********/
void intToPort(unsigned int p1, unsigned int p2) {
	
	int port;
	port = p1 * 256 + p2;
	sprintf(buf, ":%d\n", port);
	strcat(peers, buf);
	//printf(":%d\n", port);	
}
/************************************************
 ******** PREVOD CISLA NA CELOU IP ADRESU *******/
void intToIp(unsigned int ip) {
	
	struct in_addr addr;
	addr.s_addr = htonl(ip); 	
	char *s = inet_ntoa(addr);
	strcat(peers,s);
	//printf("%s", s);
}
/************************************************
 *********** PARSER ODPOVEDI OD SERVERU *********/
int parserResponse(char *response) {
	
	if(((strstr(response, "HTTP")) != NULL) && ((strstr(response, "OK")) != NULL)) 
		return 1;
	
	return 0;
}
/************************************************
 ******** PRIPOJENI, POSLANI, PRIJIMANI ********/
void connectToTracker(char **tracker) {
	
	int sock;									// socket
	int port = 80;								// port
	struct hostent *iphost;						// ip adresa
	struct sockaddr_in sin; 					// struktura socket adresy
	char *url = (char *)malloc(1);				// url hostname 
	char *hostname = (char *)malloc(1);			// url hostname
	char *host = (char *)malloc(1);				// hostname
	char *portStr = (char *)malloc(20);			// port ve stringu
	char infoHashEncoded[40+20+1] = {0};		// zakodovaen infoHash
	char *peer_id = "45234267878760032225";		// peer_id

	/* zakodovani infoHash */
	for (int k = 0; k < 20; k++) {
		infoHashEncoded[3*k] = '%';
		infoHashEncoded[3*k+1] = infoHash[2*k+0];
		infoHashEncoded[3*k+2] = infoHash[2*k+1];
	}

	//printf("# hash %s\n", infoHash);
	//printf("# HASH %s\n", infoHashEncoded);

/* ################################################################################# */
/* ########################## PARSOVANI URL ADRESY ################################# */

	int m;
    int sizeH = 1;
	int trackSize = strlen(*tracker) + 1;
    
    /* parsovani url -> odebrani http:// */
    url = (char *) realloc(url, trackSize);
    memset( url, '\0', trackSize);
    strncpy(url, (*tracker) + 7, trackSize);
    
    //printf("# URL: [%s]\n", url);

    /* parsovani cele url na prvni cast */
    if ((m = strpos(url, "/")) != -1) {
    	
    	sizeH = sizeof(char) * (m + 1);
    	host = (char *) realloc(host, sizeH);
    	memset(host, '\0', sizeH);
		strncpy(host, url, m);
	} else {

		m = strlen(url);
    	host = (char *) realloc(host, m + 1);
    	memset(host, '\0', m + 1);
		strncpy(host, url, m);
	}

    /* parsovani portu, pokud existuje (defaultne 80) */
    if ((m = strpos(host, ":")) != -1) {
    	
    	memset(portStr, '\0', 20);
		strncpy(portStr, host+m+1, strlen(host) - m - 1); 
		port = atoi(portStr);
	}

	printf("# HOST: %s\n", host);
	printf("# PORT: %d\n", port);

	/* parseruje url adresu pro pripojeni z prvni casti */
	if ((m = strpos(host, ":")) != -1) {	
		hostname = (char*) realloc(hostname, sizeof(char) * (m + 1));
		memset( hostname, '\0', sizeof(char) * (m + 1));
		strncpy(hostname, host, m);
	} else if ((m = strpos(host, "/")) != -1) {	
		hostname = (char*) realloc(hostname, sizeof(char) * (m + 1));
		memset( hostname, '\0', sizeof(char) * (m + 1));
		strncpy(hostname, host, m);
	} else {	
		hostname = (char*) realloc(hostname, sizeof(char) * (strlen(host) + 1));
		memset( hostname, '\0', sizeof(char) * (strlen(host) + 1));
		strncpy(hostname, host, strlen(host));

	}

	free(url);
	printf("# URL connect: [%s]\n", hostname);

/* ################################################################################# */
/* ############################# SESTAVENI GET REQUEST ############################# */
	
	/* konkatenace url trackeru s infohash a peer_id */
    int size = strlen(peer_id) + strlen(infoHash) + strlen(host) + 9999;
    
    char *requestToSend = (char *)malloc(size);
    memset(requestToSend, '\0', size);
	
	/* vytvoreni cele zpravy do stringu requestToSend */
	sprintf(requestToSend, "GET /announce?info_hash=%s&peer_id=%s&port=%s&downloaded=0&left=1465208&event=started&corrupt=0&key=B91519B3&event=started&numwant=200&compact=1&no_peer_id=1 HTTP/1.1\r\nHost: %s\r\nUser-Agent: uTorrent/2210(25534)\r\nConnection: Close\r\n\r\n", infoHashEncoded, peer_id, "6888", host);
	
	printf("------------- Request To Send -------------\n%s", requestToSend);
    printf("-------------------------------------------\n");

/* ################################################################################# */
/* ###################### CONNECT, GET REQUEST, GET PEER LIST ###################### */
	
	int poc = 1;
   	int rec = 0;
   	struct timeval timeoutRecv;
   	struct timeval  timeoutConn;

	timeoutRecv.tv_sec  = 15;  	// nastaveni 10 sekund timeout pro receive 
	timeoutRecv.tv_usec = 0;   

	fprintf(stdout, "--------------- KOMUNIKACE ----------------\n");

	/* cyklus, ktery obsahuje pripojeni, poslani zpravy a prijimani zpraavy      */
	/* vse se odehrava max. trikrat, pokud na poprve vse prijme jak ma tak konci */
   	while((rec == 0) && (poc <= 3)) {
		
		/* vytvoreni socketu, (rodina protokolu, spojovana spolehliva komunikace, TCP protokol) */
	    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	        fprintf(stderr, "!! Socket error: chyba pri vytvareni socketu\n"); 
	        return;
	    }

	    /* prelozeni domenoveho jmena na IP adresu */
	  	if ((iphost =  gethostbyname(hostname)) == NULL) {
	    	fprintf(stderr, "!! GetHostByName (DNS) Error: nelze prelozit hostname [%s]\n", hostname);
	    	return;
	   	}
	   	  	
		/* nastaveni options - nastaveni timeout - pro receive scoket */
	    if (setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeoutRecv, sizeof(timeoutRecv)) < 0)
	    {
	    	fprintf(stderr,"!! Sock error: chyba pri nastaveni options socketu\n"); 	
	    	return;
	    }

		poc++;
	   	int conn = 0;
	   	memset(&sin, 0, sizeof(sin)); 				
	    sin.sin_family = AF_INET;			// nastaveni family protokolu
		sin.sin_port = htons(port);  		// nastaveni cisla portu
	    memcpy(&sin.sin_addr, iphost->h_addr, iphost->h_length);	// kopie ip adresy a portu
	    
	    time ( &rawtime );
  		timeinfo = localtime ( &rawtime );
	    fprintf(stdout, "# PRIPOJOVANI na tracker [%s] v case %02d:%02d:%02d\n", inet_ntoa(sin.sin_addr), timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

 		int fl, res, valopt;
	    fd_set set;
	    socklen_t lon; 

    	/* nastaveni socketu na nonblocking a nastaveni priznaku pro pouziti casovace */
    	if( (fl = fcntl(sock, F_GETFL, 0)) < 0) {
    		fprintf(stderr,"!! FCNTL error: chyba pri nastaveni socketu (%s)\n", strerror(errno)); 	
	    	return;
	    }
    	if( (fcntl(sock, F_SETFL, fl | O_NONBLOCK)) < 0) {
    		fprintf(stderr,"!! FCNTL error: chyba pri nastaveni options socketu (%s)\n", strerror(errno));
	    	return;
    	}

	   	char responseFromServer[1024] = {0};

	   	/* ************************* CONNECT ************************* */
		/* * navazani TCP spojeni na danou ip adresu pomoci daneho portu */
		if ((conn = (connect(sock, (struct sockaddr *)&sin, sizeof(sin)))) < 0 ) {
			/* *********************************** timeout casovac pro connect ************************** *
			 * kod prevzaty ze stranky http://developerweb.net/viewtopic.php?id=3196, autor: HectorLasso  */
			if (errno == EINPROGRESS) { 
				fprintf(stderr, "# CONNECTING (timeout 10 sekund) .....\n"); 
				/* cekaci smycka, ktera se provadi dokud neni navazano spojeni, nevyprsi timeout nebo nevznikne nejaka chyba pri pripojovani */
				do { 	  
   					timeoutConn.tv_sec = 10;		// nastaveni 10 sekund timeout pro connect
    				timeoutConn.tv_usec = 0;
					FD_ZERO(&set);
    				FD_SET(sock, &set);

					res = select(sock+1, NULL, &set, NULL, &timeoutConn); 

					/* pokud nastala chyba pri connect */
					if (res < 0 && errno != EINTR) { 
						fprintf(stderr, "!! Error connecting %d - %s\n", errno, strerror(errno)); 
					  	return; 
					} else if (res > 0) { 
						// Socket selected for write 
					  	lon = sizeof(int); 
					  	if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon) < 0) { 
					  		fprintf(stderr, "!! Error in getsockopt() %d - %s\n", errno, strerror(errno)); 
					     	return;
					  	} 
					  	// Check the value returned... 
					  	if (valopt) { 
					     	fprintf(stderr, "!! Error in delayed connection() %d - %s\n", valopt, strerror(valopt)); 
					     	return;
					  	} 
						break; 
					} 
					/* pokud vyprsel timeout pro connect */
					else { 
						time ( &rawtime );
  						timeinfo = localtime ( &rawtime );
						fprintf(stderr, "!! Connection Timed Out (10s)  v case %02d:%02d:%02d\n",timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec); 
					  	return; 
					} 
				} while (1); 
			} else { 
				fprintf(stderr, "!! Connection error %d: pri pripojeni na server %s\n", conn, hostname); 
				return; 
			} 																							 
			/* ********************************** konec prezvateho kodu ********************************* */
		}
			
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
    	fprintf(stdout, "# PRIPOJENO po %dx v case %02d:%02d:%02d\n", poc-1, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		
		bzero(responseFromServer, sizeof(responseFromServer));
		fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) & ~O_NONBLOCK);	// nastaveni zpet na nonblocking

		/* ************************* SEND ************************* */	
		/* * klient odesle data na server (danou IP adresu) s navazanym TCP spojenim pomoci socketu */
	  	if (send(sock, requestToSend, strlen(requestToSend), 0) < 0 ) { 
	    	fprintf(stderr, "!! Send error: chyba pri odesilani GET na %s\n", hostname);
	    	exit(EXIT_FAILURE);
	  	}

	  	time ( &rawtime );
		timeinfo = localtime ( &rawtime );
	  	fprintf(stdout, "# ODESLANA data na [%s]  v case %02d:%02d:%02d\n", inet_ntoa(sin.sin_addr), timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		
	  	rec = 0;	// vynulani vysledku
		
		/* ************************* RECV ************************* */
		/* * klient prijme data od serveru danym IP adresou pomoci navazaneho TCP spojeni */
		if ((rec = recv(sock, responseFromServer, sizeof(responseFromServer), 0)) < 0) {
		  	fprintf(stderr, "!! Recv error: chyba pri cteni dat z [%s] (timeout 15 sekund)\n",  inet_ntoa(sin.sin_addr));
		}	
		/* pokud spojeni spadlo behem posilani a prijimani zprav */
	  	if (rec == 0) {
	  		fprintf(stderr, "!! Server %s se odpojil a neposlal zadnou zpravy\n", hostname);
	  		printf("-------------------------------------------\n");
	  	/* zpracovani zpravy od serveru */
	  	} else {
	  		time ( &rawtime );
			timeinfo = localtime ( &rawtime );
		  	fprintf(stdout, "# PRIJATA zprava od [%s] v case %02d:%02d:%02d\n", inet_ntoa(sin.sin_addr), timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		  	printf("-------- ZPRAVA od %s ---------\n%s", inet_ntoa(sin.sin_addr), responseFromServer);
		  	printf("\n-------------------------------------------\n");
		  	
		  	/* pokud server neposlal validni odpoved */
		  	if (parserResponse(responseFromServer) != 1) {

		  		int o = 0;
		  		/* vypis dany error ve zprave */
		  		if ((strstr(responseFromServer, "HTTP")) != NULL) {
		  			fprintf(stderr, "Obdrzena chybna zprava od %s [", inet_ntoa(sin.sin_addr));
			  		while(responseFromServer[o] != '\r') {
						fprintf(stderr, "%c", responseFromServer[o]);
						o++;
					}
					fprintf(stderr, "]\n");
				} else {
					fprintf(stderr, "!! Zprava od [%s] nebyla obdrzena", hostname);
				}
				
				printf("\n-----------------------------------------\n");
		  	}
		  	/* pokud server poslal validni odpoved, zpracuj ji */
		  	else {

			  	int p = 0;
			  	int flag = 0;
			  	char *responseTxt = "response.txt";
			  	bencodePeers = 1;			// flag oznacujici, ze jdeme bencodovat response od serveru
			  	FILE *responseFile = fopen(responseTxt, "w+");

			  	/* parsrovani zpravy, ulozeni pouze datove casti bez hlavicky */
			  	//if ((m = strpos(strPeers, "\r\n\r\n")) != -1) {

			  	for (int i = 0; i < rec; i++) {
			  		/* odstraneni hlavicky */
			  		if ((responseFromServer[i] == '\r') && (responseFromServer[i+1] == '\n') && (responseFromServer[i+2] == '\r') && (responseFromServer[i+3] == '\n')) {
			  			i = i + 4;
			  			p = i;
			  			flag = 1;
			  		}

			  		if (flag == 1)
						fprintf(responseFile, "%c", responseFromServer[i]);		
				}

				buf = (char *) malloc(10);
				peers = (char *) malloc(5000);
				strcpy(peers, "");
				/* pokud odpoved obsahuje potrebne peers */
				if ((strstr(responseFromServer, "peers")) != NULL) {

					m = strpos(responseFromServer, "peers");
					/* ulozeno, co je za peers */
					char *strPeers = (char *) malloc(10);
					memset(strPeers, '\0', 10);
					memcpy(strPeers, responseFromServer + m + 5, 9);
					/* osetreni, zda za peers je cislo a dvojtecka */
					if ((m = strpos(strPeers, ":")) != -1) {
						
						char *numPeers = (char *) malloc(6);
						memset(numPeers, '\0', 6);
						memcpy(numPeers, strPeers, m);

						char *err = NULL;
						strtol(numPeers, &err, 10);
						/* pokud za peers neni cislo vrat chybu */
						if (err && (*err != '\0')) {
							fprintf(stderr, "!! Server odeslal spatnou zpravu pro zpracovani\n");
							return;
						}

						free(numPeers);
					/* chybna zprava od serveru */
					} else {
						
						fprintf(stderr, "!! Server odeslal spatnou zpravu pro zpracovani\n");
						return;
					}

					free(strPeers);
					
					if (responseFile != NULL) 
						fclose(responseFile);

					/* bencode response zpravy od serveru */
					if ((trackerParser(&responseTxt)) == -1) {
						return;
					}

					printf("# sizePeers: %lld\n",sizePeers);
					printf("--------------- PEERS LIST ----------------\n%s", peers);
					printf("-------------------------------------------\n");
					
					/* dany ip adresy s portem pridej do souboru peerlist pro dany torrent */
					fprintf(peerList, "%s", peers);
				/* pokud odpoved neobsahuje peers */
				} else {
					
					if (responseFile != NULL) 
						fclose(responseFile);
					
					fprintf(stderr, "!! Server neodeslal peers ale chybovou zpravu: ");
					/* vypis chybovou zpravu na stderr */
					printFile(responseFromServer, p);
				
					printf("\n-------------------------------------------\n");
				}

			    free(peers);
	    	}
    	}

		/* ************************* CLOSE ************************* */
		/* * ruseni socketu a uzavreni spojeni 						 */
	  	if (close(sock) < 0) {
	    	fprintf(stderr, "!! Close error: chyba pri ruseni socketu\n");
	  	}
	}

	printf("# Prosel jsem %d cyklu\n", poc-1);
	printf("-------------------------------------------\n");

	if (poc > 1) {
		
		if (rec == 0)
			printf("# Server neodpovedel\n");
		else
			printf("# Server odpovedel\n");
		
		printf("-------------------------------------------\n");
	}
	
	free(host);
	free(portStr);
	free(hostname);
	free(requestToSend);
}
/* ################################################################################# */
/* #################### ZPRACOVANI JEDNOTLIVYCH HTTP TRACKERU  ##################### */
void getPeerList() {

	int i, pos1, pos2;
	int count = 0;
	char c;

	char *filename = (char *) malloc(100);
	strcpy(filename, infoHash);
	strcat(filename, ".peerlist");
	peerList = fopen(filename, "w");

	/* pokdu nebyl zadan prepinac -a */
	if (a != 3) {
		
		http_file = fopen("http_url.txt","r+");
		
		/* vypocet delky url */
		while(fscanf(http_file, "%c", &c) != EOF) { 	
			count++;
		}

		count++;

		char *http_url = (char *)malloc( sizeof(char) * count);
		
		rewind(http_file);
		fgets(http_url, count, http_file);
		
		/* pokud pole neobsahuje zadne http trackery */
		if (http_url[0] == '\0')
			fprintf(stdout, "# Zadne HTTP trackery\n");

		/* prochazeni vsech jednotlivych http trackeru */
		for(i = 0; i < http_count; i++) {
			
			pos1 = pos2 = 0;
			
			fprintf(stdout, "######################################################\n");
			fprintf(stdout, "##################### LINK %d ########################\n", i+1);
		    
		    /* bere jednotlive http trackery, ktery jsou oddeleny znaky || */
		    if ((pos2 = strpos(http_url, "||")) != -1) {
		    	
		    	/* vyparsrovani jednoho url */
		    	char *link  = (char *) malloc(pos2+1);
		    	memset(link, '\0', pos2+1);
		    	memcpy(link, http_url, pos2);
		    	pos1 = pos2 + 2;
		    	memmove(http_url, http_url+pos1, count-pos1);
		    	
		    	fprintf(stdout, "## [%s]\n",link);
		    	
		    	/* zavolani zpracovani daneho url - connect, sedn, resv */
		    	connectToTracker(&link);
		    	
		    	free(link);
	    	}	
		}

		free(http_url);

	/* byl zadan prepinac -a overridni http trackery */
	} else {
		fprintf(stdout, "######################################################\n");
		fprintf(stdout, "################# ONLY ONE LINK ######################\n");
		connectToTracker(&track_url);

	}

	free(filename);
	fclose(peerList);
}