/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include "rssfeed.h"
#include "parser.h"
#include "peerlist.h"
#include "error.h"
#include "bencode.h"

int a = 0;
char *track_url;

//########################################---NAPOVEDA---####################################################//
void getHelp() {
   	printf("\n################## NAPOVEDA ##################\n"
   		   "##"
           "## Autor: Tomas Hynek (c) 2015\n"
           "## Login: xhynek09\n"
           "##############################################\n"
           "#### -r [--rss] URL -- URL adresa RSS feedu\n"
           "#### -i [--input-announcement] filename -- uložený RSS feed v souboru\n"
           "#### -a [--tracker-annonce-url] [--tracker-announce-url] URL -- podvržené URL trackeru pro získaní peerlistu\n"
           "#### -r [--rss] URL -- URL adresa RSS feedu\n"
           "#### -t [--torrent-file] filename -- bonusovy ukol neimplementovan\n"
           "##############################################\n\n");
}

//#######################################---MAIN-PROGRAM---#################################################//
int main(int argc, char *argv[])  
{
	int i;
	int cnt = 0;
	int flag = 0;
	char *rss_url;
	char *rss_file;

	/* zpracovani argumentu */
	for (i=1; i < argc; i++) {

		if ((strcmp("-r",argv[i]) == 0) || (strcmp("--rss",argv[i]) == 0)) {
			
			i++;
			cnt++;
			rss_url = argv[i];
			flag = 1;
			//printf("-r %s\n", rss_url);

	    } else if ((strcmp("-i",argv[i]) == 0) || (strcmp("--input-announcement",argv[i]) == 0)) {
	    	
	    	i++;
	    	cnt++;
	    	rss_file = argv[i];			
			flag = 2;
			//printf("-i %s\n", rss_file);
												
	    } else if ((strcmp("-a",argv[i]) == 0) || (strcmp("--tracker-annonce-url",argv[i]) == 0) || (strcmp("--tracker-announce-url",argv[i]) == 0)) {
	    	
	    	i++;
	    	track_url = argv[i];
			a = 3;
			//printf("-a %s\n", track_url);

	    } else if ((strcmp("-t",argv[i]) == 0) || (strcmp("--torrent-file",argv[i]) == 0)) {
	    	
	    	if ((Error(2)) != 0)
        		exit(EXIT_FAILURE);	

	    } else if ((strcmp("-h",argv[i]) == 0) || (strcmp("--help",argv[i]) == 0)) {
	    	
	    	getHelp();
			return 0;

	    } else {

	    	if ((Error(0)) != 0)
        		exit(EXIT_FAILURE);

	    }

	    if (cnt > 1){
	    	
	    	if ((Error(0)) != 0)
        		exit(EXIT_FAILURE);
        }
	}

	/*  zpracovani pri argumentu -r | -rss 
		na vstupu URL adresa RSS feedu */
	if (flag == 1) {		
		
		getRSS(&rss_url);					// stazeni XML souboru
		xmlParser("movies_announce.xml");	// parsrovani, zpracovani http trackeru
	
	}
	/*  zpracovani pri argumentu -i | --input-announcement 
		na vstupu RSS feed soubor */
	else if (flag == 2) {	
	
		xmlParser(rss_file);	// parsrovani, zpracovani http trackeru
	
	}
	
	printf("# Koncim zpracovani\n");

	return 0;
}
