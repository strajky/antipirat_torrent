/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/
#define FILE_IO

#include <stdio.h>
#include <string.h>
#include <regex.h>
#ifdef FILE_IO
#include <sys/stat.h>
#include <stdlib.h>
#endif
#include <curl/curl.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "parser.h"
#include "rssfeed.h"
#include "error.h"
#include "bencode.h"
#include "peerlist.h"

int flag = 0;
int torrFlag = 1;
char* infoHash;
FILE *announce_list;
FILE *trackerUrl_list;


/* Pomocna funkce pro vyjmuti jmena trackeru z URL adresy */
int strpos(char *haystack, char *needle) {
  
   char *p = strstr(haystack, needle);
  
   if (p)
      return p - haystack;
  
   return -1;
}

/* ######################################################################################## */
/* ########## FUNKCE ZPRACOVAVAJICI XML SOUBOR, VYTHANE POUZE POTREBNE INFORMACE ########## */
void getXML(xmlNode *node) {	
	
	char *track_url;
    xmlNode *cur_node = NULL;
    xmlChar *content;
    int noAuthor = 0;

    /* prochazeni jednotlivych elemntu v XML souboru a vlozeni do souboru */
    for (cur_node = node; cur_node; cur_node = cur_node->next) {
      	
      	if (cur_node->type == XML_ELEMENT_NODE) {
      		
      		if (strcmp((char *)cur_node->name,"channel") == 0) {
      			flag = 1;
      		} else if ((strcmp((char *)cur_node->name,"title") == 0) && (flag == 1)) { 
				content = xmlNodeGetContent(cur_node);
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if ((strcmp((char *)cur_node->name,"link") == 0)  && (flag == 1)) { 
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if ((strcmp((char *)cur_node->name,"description") == 0)  && (flag == 1)) { 
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n\n", (char *)cur_node->name, content);
				flag =0;	
			} else if (strcmp((char *)cur_node->name,"title") == 0) { 
				content = xmlNodeGetContent(cur_node);						// ulozi obsah daneho elementu
				fprintf (announce_list, "\n%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"category") == 0) { 
				noAuthor = 1;	// pozor, dlasi ma byt author, nastaveni na 1
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"author") == 0) { 
				noAuthor = 0;	// author je, nastavit 0
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"link") == 0) {	
				if (noAuthor == 1) { fprintf (announce_list, "author: \n"); }	// pokud author neni, dopln prvek s prazdnym retezcem
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"pubDate") == 0) { 
				content = xmlNodeGetContent(cur_node);	
				fprintf (announce_list, "%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"infoHash") == 0) { 
				content = xmlNodeGetContent(cur_node);	
				if (torrFlag == 1) { infoHash = (char *)content; }
				fprintf (announce_list, "torrent:%s: %s\n", (char *)cur_node->name, content);
			} else if (strcmp((char *)cur_node->name,"fileName") == 0) { 
				content = xmlNodeGetContent(cur_node);		
				fprintf (announce_list, "torrent:%s: %s\n", (char *)cur_node->name, content);
			} else if ((strcmp((char *)cur_node->name,"enclosure") == 0) && (torrFlag == 1)) {
				content = xmlGetProp(cur_node, (xmlChar *)"url");	
				track_url = (char *)content;
				fprintf (trackerUrl_list, "%s", track_url);		// ulozeni prvni url torrentu
				torrFlag = 0;									// ulozil jsme jej, priste uz nechci
			}

			getXML(cur_node->children);		// jdi na dalsi prvek
      	}
    }
 }
/* ######################################################################################## */
/* ############################## STAZENI TORRENTU, BENCODING TORRENTU #################### */
int downloadTracker() {

	int count = 1;
	int pos = 0;
	char c;
	char *trackerFilename = (char *)malloc(sizeof(char) * 200);

	trackerUrl_list = fopen("trackerUrl.txt","r");
	
	/* vypocet delky url */
	while(fscanf(trackerUrl_list, "%c", &c) != EOF) {	
		count++;
	}

	char track_url[count];
	char *tracker_Url;

	rewind(trackerUrl_list);						// reset pozice v souboru
	fgets(track_url, count, trackerUrl_list);		// nacteni url do stringu
	
	if (trackerUrl_list != NULL) 
		fclose(trackerUrl_list);

	int size = count-pos;
	trackerFilename = realloc(trackerFilename, size+8);
	tracker_Url = (char *)track_url;
	tracker_Url = curl_unescape(tracker_Url, strlen(tracker_Url));

	/* vyjmuti nazvu torrentu pro pozdeji ulozeni */
	if ((pos = strpos(tracker_Url, "=[kat.cr]")) == -1) {	
		fprintf(stderr, "!! Nelze vytvorit nazev torrentu pro jeho pozdejsi ulozeni\n");
		exit(EXIT_FAILURE);
	} else {
		pos = pos + 9;			
		strncpy(trackerFilename, tracker_Url+pos, size);
		strcat(trackerFilename, ".torrent");
	}

	printf("# trackerFilename: %s\n", trackerFilename);
	printf("# infoHash: %s\n", infoHash);

	FILE *trackerFile;
	CURL *curl;
  	CURLcode result;

  	/* inicializace curl */
  	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	fprintf(stdout, "# STAHOVANI torrentu\n");

	/* stazeni torrent souboru */
	if(curl) {

		trackerFile = fopen(trackerFilename, "w");

		/* nastaveni endconding, kam data ulozit a kam se pripojit a odkud je cist */
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING , "gzip"); 
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, trackerFile); 
		curl_easy_setopt(curl, CURLOPT_URL, tracker_Url);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		/* do result se ulozi return code  */
		result = curl_easy_perform(curl);	

		/* kontrola chyb*/ 
		if(result != CURLE_OK) {
			fprintf(stderr, "!! Stazeni torrentu bylo neuspesne -> curl_easy_perform() failed: %s\n",curl_easy_strerror(result));
			exit(EXIT_FAILURE);
		}

		/* cisteni */ 
		curl_easy_cleanup(curl);

		if (trackerFile != NULL) 
			fclose(trackerFile);
	}

	curl_global_cleanup();	

	/* bencode torrentu */
	if ((trackerParser(&trackerFilename)) == -1){
		return -1;
	}

	free(trackerFilename);

	return 0;
}
/* #################################################################################### */
/* ** Nasledujici dve funkce jsou zkopirovane podle souboru test.c od Mike Frysinger ** */
/* ** Vyuzivaji knihovnu bencode.h | Written by: Mike Frysinger <vapier@gmail.com>   ** */
/* #################################################################################### */
char *read_file(const char *file, long long *len) {
#ifdef FILE_IO
	struct stat st;
	char *ret = NULL;
	FILE *fp;

	if (stat(file, &st))
		return ret;
	*len = st.st_size;

	fp = fopen(file, "r");
	if (!fp)
		return ret;

	ret = malloc(*len);
	if (!ret)
		return NULL;

	fread(ret, 1, *len, fp);

	fclose(fp);

	return ret;
#else
	return NULL;
#endif
}
/* ################################################################################# */
/* ########################### BENCODE TORRENT | RESPONSE ########################## */
int trackerParser(char **trackerFileName) {

	setbuf(stdout, NULL);
	char *buf;
	long long len;
	be_node *n;

	/* nacteni celeho souboru pro bencodovani do bufferu */
	buf = read_file(*trackerFileName, &len);
	if (!buf) {
		buf = *trackerFileName;
		len = strlen(*trackerFileName);
	}

	//printf("# DECODING: %s\n", *trackerFileName);
	//printf("# BUF:      %s\n", buf);
	//printf("# LENGTH:   %lld\n", len);

	/* bencodovani */
	n = be_decoden(buf, len);
	if (n) { 
		be_dump(n);
		be_free(n);
	} else {
		fprintf(stderr, "!! Bencode Error: parsovani torrentu/response zpravy bylo neuspesne\n");
		return -1;
	}

	#ifdef FILE_IO
		if (buf != *trackerFileName)
			free(buf);
	#endif

	printf("# FILE DECODE ...\n");

	return 0;
}
/* ################################################################################### */
/* # PARSROVANI XML, STAHNUTI A ZPRACOVANI TORRENTU, ZPRACOVANIT VSECH HTTP TRACKERU # */
void xmlParser(char *rss_file) {

	xmlDoc *doc = NULL;
    xmlNode *root_elem = NULL;
    
    announce_list = fopen("movies_announce_list.txt","w");
    trackerUrl_list = fopen("trackerUrl.txt","w");

    /* parsovani XML souboru */
    if ((doc = xmlReadFile(rss_file, NULL, 0)) == NULL) {
    	fprintf(stderr, "!! Nastala chyba pri XMLReadFile\n");
		exit(EXIT_FAILURE);
	}

    /* ziskani root elementu */
    root_elem = xmlDocGetRootElement(doc);
    
    /* parsovani XML souboru */
    getXML(root_elem);
    
    if (trackerUrl_list != NULL) 
		fclose(trackerUrl_list);
    
    fprintf(stdout, "# ZPRACOVANO XML\n");

    /* zpracovani torrentu */
    if ((downloadTracker()) == -1) {
		exit(EXIT_FAILURE);
	}

    fprintf(stdout, "# STAZENO\n");
    fprintf(stdout, "# ZISKAVANI peerlistu\n");

    /* zpracovani http trackeru, get a response zpravy, peerlistu */
    getPeerList();
    
    printf("######################################################\n");
    fprintf(stdout, "# ZISKAN peerlist\n");
    
    xmlFreeDoc(doc);       // cisteni dokumentu
    xmlCleanupParser();    // globalni cisteni
    
    if (announce_list != NULL) 
		fclose(announce_list);
}