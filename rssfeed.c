/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include "rssfeed.h"
#include "error.h"
/* ########################## STAZENI XML SOUBORU ########################## */
void getRSS(char **rss_url) {

	CURL *curl;
  	CURLcode result;
  	FILE *xml_file; 

  	if ((xml_file = fopen("movies_announce.xml", "w+")) == NULL) {
        if ((Error(1)) != 0) {
        	fclose(xml_file);
        	exit(EXIT_FAILURE);
        }
	}

	/* inicializace curl */
  	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();

	fprintf(stdout, "# STAHOVANI XML soubor z adresy %s\n", *rss_url);

	/* stazeni xml souboru */
	if(curl) {
		
		/* nastaveni endconding, kam data ulozit a kam se pripojit a odkud je cist */
		curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING , "gzip"); 
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, xml_file); 
		curl_easy_setopt(curl, CURLOPT_URL, *rss_url);
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		/* do result se ulozi return code  */ 
		result = curl_easy_perform(curl);

		/* kontrola chyb*/ 
		if(result != CURLE_OK){
			fprintf(stderr, "!! Stazeni XML bylo neuspesne -> curl_easy_perform() failed: %s\n",curl_easy_strerror(result));
			exit(EXIT_FAILURE);
		}

		/* cisteni */ 
		curl_easy_cleanup(curl);
	}

	/* cisteni */
	curl_global_cleanup();	

	fclose(xml_file);	
}
