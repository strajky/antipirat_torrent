/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#ifndef xmlparser
#define xmlparser

#include <stdio.h>
#include <stdlib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "bencode.h"
#include "parser.h"

extern char* infoHash;
extern int strpos(char *haystack, char *needle);

void xmlParser(char *rss_file);
int downloadTracker();
void getXML(xmlNode *node);
char *read_file(const char *file, long long *len);
int trackerParser(char **trackerFileName);

#endif