/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#ifndef peerlist
#define peerlist
#include "parser.h"

extern char *infoHash;
extern char *peers;
extern int http_count;
extern FILE *http_file;
extern int strpos(char *haystack, char *needle);

void getPeerList();
void connectToTracker(char **tracker);
void intToIp(unsigned int ip);
void intToPort(unsigned int p1, unsigned int p2);
unsigned int hexToInt(char *hexStr);

#endif