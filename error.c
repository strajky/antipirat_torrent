/*Projekt: Analýza BitTorrent služby KickassTorrents
  Autor: Tomas Hynek
  Login: xhynek09
  Rocnik: VUT FIT 3BIT
  Datum: 11.10.2015
  Knihovny: curl-7.45.0, libxml2-2.7.2
*/

#include <stdio.h>
#include <stdlib.h>
#include "error.h"


int Error(int num) {
	
	switch (num) {
		case 0:	 fprintf(stderr, "!! Chybove hlaseni - zadali jste spatne argumenty || Pro vypis napovedy pouzijte -h [--help]\n"); 
				 return 1; break;
		case 1:	 fprintf(stderr, "!! File error - %d\n",num); return 1; break;
		case 2:	 fprintf(stderr, "!! Bonusova uloha neni implementovana || Pro vypis napovedy pouzijte -h [--help]\n"); 
				 return 1; break;
		default: fprintf(stderr, "!! Chyba nevim\n"); return 0;
	}

	return 0;
}
